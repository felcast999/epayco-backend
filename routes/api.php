<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace'     => 'Api',
], function(){
    Route::post('register','AuthController@register');
    Route::post('login','AuthController@login');

    Route::post('recharge','WalletController@recharge');
    Route::post('pay','WalletController@pay');
    Route::post('confirm-payment/{token}','WalletController@confirmPayment');
    Route::post('check-balance','WalletController@checkBalance');


});