<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;
use LaravelDoctrine\Migrations\Schema\Table;
use LaravelDoctrine\Migrations\Schema\Builder;

class Version20221123132223 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        (new Builder($schema))->create('transactions', function (Table $table) {
            $table->increments('id');
            $table->bigInteger('wallet_id',false,true);
            $table->foreign('wallets','wallet_id','id',['persist','remove','merge']);
            $table->bigInteger('status_id',false,true);
            $table->foreign('statuses','status_id','id',['persist','remove','merge']);
            $table->decimal('amount',5,2);
            $table->nullableTimestamps();
        });
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        (new Builder($schema))->drop('transactions');
    }
}
