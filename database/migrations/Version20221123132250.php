<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;
use LaravelDoctrine\Migrations\Schema\Table;
use LaravelDoctrine\Migrations\Schema\Builder;

class Version20221123132250 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        (new Builder($schema))->create('access_tokens', function (Table $table) {
            $table->increments('id');
            $table->string('token');
            $table->bigInteger('transaction_id',false,true);
            $table->foreign('transactions','transaction_id','id',['persist','remove','merge']);
            $table->bigInteger('status_id',false,true);
            $table->foreign('statuses','status_id','id',['persist','remove','merge']);
            $table->nullableTimestamps();
        });
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        (new Builder($schema))->drop('access_tokens');
    }
}
