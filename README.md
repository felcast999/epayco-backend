<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Epayco Backend

## Pasos para instalar:



- Clonar respositorio
- Ejecutar `composer install`
- Configurar archivo .env
- Crear variables de entorno `php artisan key:generate`
- Añadir `APP_HOST="http://localhost:8080"` Por defecto, el backend usara el puerto 8080 y el gateway el puerto 8000
- Ejecutar migraciones `php artisan doctrine:migrations:migrate` 
- Realizar Mapeo de entidades `php artisan doctrine:schema:update`
- Instalar passport `php artisan passport:install`
- Ejecutar Seeders `php artisan db:seed`
- Definir credenciales para envio de correos, por defecto se utliza mailtrap
- Levantar el servidor `php artisan serve --port=8080`
- Video Explicativo https://www.youtube.com/watch?v=aXOzrpJdY70

## Que se implemento

* Doctrine
* Dependency injection
* Api Gateway
* JWT
* Transactions
