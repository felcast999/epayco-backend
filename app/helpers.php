<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

if( ! function_exists("generateToken") )
{
    function generateToken()
    {

        $data = random_int(100000, 999999);
        return $data;
    
    }
}

