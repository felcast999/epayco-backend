<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;

/**
 * @ORM\Entity
 * @ORM\Table(name="wallets")
 */
class Wallet
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

     /**
     * @ORM\Column(type="bigint")
     */
    protected $user_id;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $amount;

    /**
     * @ORM\Column(type="datetime",nullable=true)
    */
    protected $created_at;

    /**
    * @ORM\Column(type="datetime",nullable=true)
    */
    protected $updated_at;

    
    public function __construct(
      $user_id,
      $amount
    )
    {
        $this->user_id = $user_id;
        $this->amount = $amount;
        $this->created_at = Carbon::now();

    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

}