<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;

/**
 * @ORM\Entity
 * @ORM\Table(name="statuses")
 */
class Status
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

     /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="datetime",nullable=true)
    */
    protected $created_at;

    /**
    * @ORM\Column(type="datetime",nullable=true)
    */
    protected $updated_at;

    
    public function __construct(
      $name
    )
    {
        $this->name = $name;
        $this->created_at = Carbon::now();

    }



}