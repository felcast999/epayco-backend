<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use LaravelDoctrine\ORM\Auth\Authenticatable;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use LaravelDoctrine\ORM\Notifications\Notifiable;
use Carbon\Carbon;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements AuthenticatableContract
{

    use Authenticatable;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

     /**
     * @ORM\Column(type="string")
     */
    protected $document_number;

    /**
     * @ORM\Column(type="string")
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $last_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $phone;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $email_verified_at;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

  /**
     * @ORM\Column(type="datetime",nullable=true)
    */
    protected $created_at;

    /**
    * @ORM\Column(type="datetime",nullable=true)
    */
    protected $updated_at;

    
    public function __construct(
      $document_number,
      $first_name,
      $last_name,
      $email,
      $phone,
      $password
    )
    {
        $this->document_number = $document_number;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = bcrypt($password);
        $this->created_at = Carbon::now();

    }


    public function getKey() {
      return $this->getId();
    }
}