<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;

/**
 * @ORM\Entity
 * @ORM\Table(name="transactions")
 */
class Transaction
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

     /**
     * @ORM\Column(type="bigint")
     */
    protected $wallet_id;

    /**
     * @ORM\Column(type="bigint")
     */
    protected $status_id;

     /**
     * @ORM\Column(type="decimal")
     */
    protected $amount;

    /**
     * @ORM\Column(type="datetime",nullable=true)
    */
    protected $created_at;

    /**
    * @ORM\Column(type="datetime",nullable=true)
    */
    protected $updated_at;

    
    public function __construct(
      $wallet_id,
      $status_id,
      $amount
    )
    {
        $this->wallet_id = $wallet_id;
        $this->status_id = $status_id;
        $this->amount = $amount;
        $this->created_at = Carbon::now();

    }

    //getters

    public function getId()
    {
        return $this->id;
    }

}