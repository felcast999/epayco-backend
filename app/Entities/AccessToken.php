<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;

/**
 * @ORM\Entity
 * @ORM\Table(name="access_tokens")
 */
class AccessToken
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

     /**
     * @ORM\Column(type="bigint")
     */
    protected $transaction_id;

    /**
     * @ORM\Column(type="string")
     */
    protected $token;

    /**
     * @ORM\Column(type="bigint")
     */
    protected $status_id;

    /**
     * @ORM\Column(type="datetime",nullable=true)
    */
    protected $created_at;

    /**
    * @ORM\Column(type="datetime",nullable=true)
    */
    protected $updated_at;

    public function __construct(
      $transaction_id,
      $token,
      $status_id
    )
    {
        $this->transaction_id = $transaction_id;
        $this->token = $token;
        $this->status_id = $status_id;
        $this->created_at = Carbon::now();



    }

    public function setStatus($status)
    {
        $this->status_id = $status;
    }

}