<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\TokenEmail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Wallet\{RechargeRequest,PayRequest,ConfirmPaymentRequest,CheckBalanceRequest};
use App\Entities\{Wallet,Transaction,Status,AccessToken};
use Doctrine\ORM\EntityManagerInterface;
use DB;
use Exception;


class WalletController extends Controller
{

    private $entity_manager;

    public function __construct(EntityManagerInterface $entity_manager)
    {
        $this->entity_manager=$entity_manager;
    }


    public function recharge(RechargeRequest $request)
    {

    
        try {
            $resp= DB::transaction(function () use ($request) {
  
                  $wallet=new Wallet(
                      Auth::user()->id,
                      $request->get('amount')
                  );
                  $this->entity_manager->persist($wallet);
                  $this->entity_manager->flush();
                  
                  return true;
              });
  
              if($resp)
              {
                  return response()->json(
                      [
                          "success"=>true,
                          "cod_error"=>00,
                          "message_error"=>"Recarga exitosa!"
                      ]);
              }
  
              return response()->json(
                  [
                      "success"=>true,
                      "cod_error"=>501,
                      "message_error"=>"Ha ocurrido un error"
                  ]);
  
          } catch (Exception $e) {
              return response()->json(
                  [
                      "success"=>false,
                      "cod_error"=>$e->getCode(),
                      "message_error"=>$e->getMessage()
                  ]);
          }
     
    }


    public function pay(PayRequest $request)
    {

        try {
            $resp= DB::transaction(function () use ($request) {
  
                $em=app('em');
                $result = $em->createQueryBuilder();
                $statuses = $result->select('q')
                        ->from(Status::class, 'q')
                        ->where('q.name= :name')
                        ->setParameters([':name' =>'active'])
                        ->getQuery()
                        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

                $status_id=$statuses[0]["id"];

                $transaction=new Transaction(
                      $request->get('wallet_id'),
                      $status_id,
                      $request->get('amount')
                  );
                $this->entity_manager->persist($transaction);
                $this->entity_manager->flush();

                $token=generateToken();

                $access_token= new AccessToken(
                    $transaction->getId(),
                    $token,
                    $status_id
                );
                
                //Send email
                Mail::to(Auth::user()->email)->send(new TokenEmail($token));

                $this->entity_manager->persist($access_token);
                $this->entity_manager->flush();  
                
                
                return $token;
              });
  
              if(!empty($resp))
              {
                  return response()->json(
                      [
                          "success"=>true,
                          "cod_error"=>00,
                          "data"=>["token"=>$resp],
                          "message_error"=>"Pago exitoso, revise su correo"
                      ]);
              }
  
              return response()->json(
                  [
                      "success"=>true,
                      "cod_error"=>501,
                      "message_error"=>"Ha ocurrido un error"
                  ]);
  
          } catch (Exception $e) {
              return response()->json(
                  [
                      "success"=>false,
                      "cod_error"=>$e->getCode(),
                      "message_error"=>$e->getMessage()
                  ]);
          }

  

    }

    public function confirmPayment(ConfirmPaymentRequest $request,$token)
    {
        try {
            $resp= DB::transaction(function () use ($request,$token) {
  

            $qb = $this->entity_manager->createQueryBuilder();

            $transaction=$qb
            ->select('a')
            ->from(Transaction::class, 'a')
            ->leftJoin(
                'App\Entities\AccessToken',
                'u',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'a.id = u.transaction_id'
            )
            ->where('u.token= :token')
            ->setParameters([':token' =>$token])
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

            $qb_2 = $this->entity_manager->createQueryBuilder();


            $record = $qb_2
                ->select('a')
                ->from(Wallet::class, 'a')
                ->leftJoin(
                    'App\Entities\Transaction',
                    'u',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'a.id = u.wallet_id'
                )
                ->leftJoin(
                    'App\Entities\AccessToken',
                    'c',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'c.transaction_id = u.id'
                )
                ->where('c.token= :token')
                ->setParameters([':token' =>$token])
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

                $qb_3 = $this->entity_manager->createQueryBuilder();

                $ac = $qb_3
                ->select('a')
                ->from('App\Entities\AccessToken', 'a')
                ->where('a.token= :token')
                ->setParameters([':token' =>$token])
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

            

            $wallet = $this->entity_manager->getRepository(Wallet::class)->find($record["id"]);
            
            $total=$record["amount"]-$transaction["amount"];
                    
            $wallet->setAmount($total);
            $this->entity_manager->flush();  

            $qb_4 = $this->entity_manager->createQueryBuilder();
            $status = $qb_4->select('s')
                    ->from('App\Entities\Status', 's')
                    ->where('s.name= :status')
                    ->setParameters([':status' =>'inactive'])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

            $access_token = $this->entity_manager->getRepository('App\Entities\AccessToken')->find($ac["id"]);
            $access_token->setStatus($status["id"]);

            $this->entity_manager->flush();  


            return true;
            });

            if($resp)
            {
                return response()->json(
                    [
                        "success"=>true,
                        "cod_error"=>00,
                        "message_error"=>"Pago confirmado"
                    ]);
            }

            return response()->json(
                [
                    "success"=>true,
                    "cod_error"=>501,
                    "message_error"=>"Ha ocurrido un error"
                ]);


        } catch (Exception $e) {
            return response()->json(
                [
                    "success"=>false,
                    "cod_error"=>$e->getCode(),
                    "message_error"=>$e->getMessage()
                ]);
        }

      
    }


    public function checkBalance(CheckBalanceRequest $request)
    {

        try {

        $qb = $this->entity_manager->createQueryBuilder();
        $user_id=Auth::user()->id;

        $wallet=$qb
        ->select('w')
        ->from(Wallet::class, 'w')
        ->leftJoin(
            'App\Entities\User',
            'u',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'w.user_id = u.id'
        )
        ->where('u.document_number= :document_number')
        ->andWhere('u.phone= :phone')
        ->andWhere('u.id= :id')
        ->setParameters([':document_number' =>$request->document_number,':phone'=>$request->phone,':id'=>$user_id])
        ->getQuery()
        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

        return response()->json(
            [
                "success"=>true,
                "data"=>["Balance"=>$wallet["amount"]],
                "cod_error"=>00,
                "message_error"=>"Exito"
            ]);

        } catch (Exception $e) {
            return response()->json(
                [
                    "success"=>false,
                    "cod_error"=>$e->getCode(),
                    "message_error"=>$e->getMessage()
                ]);
        }
        


    }
 
}
