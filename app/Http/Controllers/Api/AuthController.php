<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\{LoginRequest,RegisterRequest};
use Doctrine\ORM\EntityManagerInterface;
use App\Entities\{User};
use DB;
use Exception;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    
    private $entity_manager;

    public function __construct(EntityManagerInterface $entity_manager)
    {
        $this->entity_manager=$entity_manager;
    }


    
    public function login(LoginRequest $request)
    {   
     

        $credentials = request(['email', 'password']);

        if(!Auth::guard('web')->attempt($credentials))
        {
            return response()->json( [
                "success"=>false,
                "cod_error"=>401,
                "message_error"=>"unauthorized"
            ], 401);
        }
        $qb = $this->entity_manager->createQueryBuilder();

        $user=$qb
        ->select('u')
        ->from(User::class, 'u')
        ->where('u.email= :email')
        ->andWhere('u.email= :email')
        ->setParameters([':email' =>$request->email])
        ->getQuery()
        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];
      
      return response()->json(
        [
            "success"=>true,
            "data"=>$user,
            "cod_error"=>00,
            "message_error"=>"Exito"
        ],200);

    }



    public function register(RegisterRequest $request)
    {   
        try {
          $resp= DB::transaction(function () use ($request) {

                $user=new User(
                    $request->get('document_number'),
                    $request->get('first_name'),
                    $request->get('last_name'),
                    $request->get('email'),
                    $request->get('phone'),
                    $request->get('password'),
                    $request->get('password'),

                );
                $this->entity_manager->persist($user);
                $this->entity_manager->flush();
                
                return true;
            });

            if($resp)
            {
                return response()->json(
                    [
                        "success"=>true,
                        "cod_error"=>00,
                        "message_error"=>"Exito"
                    ]);
            }

            return response()->json(
                [
                    "success"=>true,
                    "cod_error"=>501,
                    "message_error"=>"Ha ocurrido un error"
                ]);

        } catch (Exception $e) {
            return response()->json(
                [
                    "success"=>false,
                    "cod_error"=>$e->getCode(),
                    "message_error"=>$e->getMessage()
                ]);
        }
        

    
 

    }

}
