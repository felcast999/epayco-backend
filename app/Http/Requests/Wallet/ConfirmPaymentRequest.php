<?php

namespace App\Http\Requests\Wallet;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Doctrine\ORM\EntityManagerInterface;
use App\Entities\{AccessToken};

class ConfirmPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {


        return 
        [
        ];
    }

        public function withValidator($validator)
    {
        $validator->after(function ($validator) 
        {
            $em=app('em');
            $result = $em->createQueryBuilder();
            $record = $result->select('q')
                    ->from(AccessToken::class, 'q')
                    ->where('q.token= :token')
                    ->setParameters([':token' =>$this->route('token')])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if(count($record))
        {

            $result = $em->createQueryBuilder();
            $status = $result->select('s')
                    ->from('App\Entities\Status', 's')
                    ->where('s.name= :status')
                    ->setParameters([':status' =>'inactive'])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];


            if ($record[0]["status_id"]==$status["id"]) 
            {
                $validator->errors()->add('error', 'The token has expired');
            }
        }else
        {
            $validator->errors()->add('error', 'Invalid token');

        }

        });
    }
}
