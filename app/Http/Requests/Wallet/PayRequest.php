<?php

namespace App\Http\Requests\Wallet;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\ValidDocument;
use Doctrine\ORM\EntityManagerInterface;
use App\Entities\{Wallet};

class PayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {


        return [
            'wallet_id' =>  
            [                                                                  
                'required',                                                            
            ],
            'amount'=> 
            [                                                                  
                'required',                                                          
            ]
        ];
    }

        public function withValidator($validator)
    {
        $validator->after(function ($validator) 
        {
            $em=app('em');
            $result = $em->createQueryBuilder();
            $record = $result->select('q')
                    ->from(Wallet::class, 'q')
                    ->where('q.id= :id')
                    ->setParameters([':id' =>$this->request->get( 'wallet_id' )])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if(count($record))
        {
            if ($record[0]["amount"]<$this->request->get( 'amount' )) {
                $validator->errors()->add('error', 'The wallet does not have enough money');
            }
        }else
        {
            $validator->errors()->add('error', 'The wallet does not exist');

        }

        });
    }
}
