<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'document_number' => 'required|string|min:2|max:50',
            'first_name'=>'required|string|min:2|max:50',
            'last_name'=>'required|string|min:2|max:50',
            'email'=>[
                "required",
                "nullable",
                "email:filter",
                "unique:App\Entities\User,email"
            ],
            'phone'=>'required',
            'password'=>[
                "required",
                "min:8",
                "confirmed"
            ]

        ];
    }
}
