<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\Validation\DataAwareRule;
use App\Entities\{User};
use Illuminate\Support\Facades\Auth;

class ValidDocument implements DataAwareRule,Rule
{
    protected $data = [];
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $em;

    public function __construct()
    {
    }

    public function setData($data)
    {
        $this->data = $data;
 
        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $user_id=Auth::user()->id;
        $em=app('em');
        $result = $em->createQueryBuilder();
        $record = $result->select('q')
                ->from(User::class, 'q')
                ->where('q.document_number= :document_number')
                ->andWhere('q.phone= :phone')
                ->andWhere('q.id= :id')
                ->setParameters([':document_number' => $this->data['document_number'],':phone'=>$this->data['phone'],':id'=>$user_id])
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        return (count($record))?true:false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Document and phone does not match with our records ';
    }
}
